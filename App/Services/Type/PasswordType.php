<?php

namespace App\Services\Type;

class PasswordType extends AbstractFormElement
{
    public function __construct($fieldName, array $options)
    {
        $this->htmlElement = "<label for='{$fieldName}'>{$options['label']}</label>";
        $this->htmlElement .= "<input name='{$fieldName}' type='password' class='form-control {$options['class']}' id='{$fieldName}'>";

        if (isset($options['confirm'])) {
            $this->htmlElement .= "</div><div class='form-group'>";
            $this->htmlElement .= "<label for='confirm_{$fieldName}'>{$options['confirm']['label']}</label>";
            $this->htmlElement .= "<input name='confirm_{$fieldName}' type='password' class='form-control {$options['class']}' id='confirm_{$fieldName}'>";
        }
    }
}