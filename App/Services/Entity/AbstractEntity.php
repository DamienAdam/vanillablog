<?php

namespace App\Services\Entity;

use App\Services\Utilities\StringUtilities;

/**
 * Class AbstractEntity
 *
 * Base class for all entities
 * Provides basic method like hydratation
 *
 * @package App\Services\Entity
 */
class AbstractEntity
{
    /**
     * Will map array values to instantiated entity using array key to match object attributes
     *
     * @param array $attributes
     */
    public function hydrate(array $attributes)
    {
        foreach ($attributes as $key => $value) {

            $method = 'set' . ucfirst(StringUtilities::snakeToPascalCase($key));

            if (method_exists($this, $method)) {
                    $this->$method($value);
            }
        }
    }

}