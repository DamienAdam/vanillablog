<?php

namespace App\Services\Mailer\Mailjet;

class MJAttachment
{
    public $contentType;

    public $fileName;

    public $content;
}