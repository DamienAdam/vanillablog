<?php

namespace App\Domain\Comment\Manager;

use App\Core\Application;
use App\Domain\Comment\Entity\Comment;
use App\Domain\Comment\Entity\CommentStatus;
use App\Domain\Comment\Repository\CommentRepository;
use App\Domain\Comment\Repository\CommentStatusRepository;
use App\Domain\User\Entity\User;
use Exception;

class CommentManager
{
    private $commentRepository;

    public function __construct()
    {
        $this->commentRepository = new CommentRepository();
    }

    public function findAll($className, array $options, array $joins = null)
    {
        return $this->commentRepository->findAll($className, $options, $joins);
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function findFirstFromId($id)
    {
        return $this->commentRepository->findFirst(Comment::class, [
            'where' => [
                ['field' => 'id',
                'operator' => '=',
                'value' => $id]
            ]
        ]);
    }

    public function delete($comment)
    {
        $this->commentRepository->delete($comment);
    }

    /**
     * @param Comment $comment
     * @throws Exception
     */
    public function moderate(Comment $comment)
    {
        $commentStatusRepository = new CommentStatusRepository();

        /** @var CommentStatus $commentStatus */
        $moderateStatus = $commentStatusRepository->findFirst(CommentStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => 'moderated']
        ]]);

        $this->commentRepository->updateObject(Comment::class, $comment->getId(), ['comment_status_id'], [$moderateStatus->getId()]);
    }

    /**
     * @param $comment
     * @throws Exception
     */
    public function validate(Comment $comment)
    {
        $commentStatusRepository = new CommentStatusRepository();

        /** @var CommentStatus $commentStatus */
        $validStatus = $commentStatusRepository->findFirst(CommentStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => 'valid']
        ]]);

        $this->commentRepository->updateObject(Comment::class, $comment->getId(), ['comment_status_id'], [$validStatus->getId()]);
    }

    public function createComment($postId, $content)
    {
        $commentStatusRepository = new CommentStatusRepository();

        /** @var CommentStatus $commentStatus */
        $pendingStatus = $commentStatusRepository->findFirst(CommentStatus::class, ['where' => [
            ['field' => 'status_name',
                'operator' => '=',
                'value' => 'pending']
        ]]);

        $comment = new Comment();
        $comment->setContent($content);
        $comment->setUserId(Application::$request->session('user')->getId());
        $comment->setPostId($postId);
        $comment->setCommentStatusId($pendingStatus->getId());

        $this->commentRepository->create($comment);
    }

    public function deleteAll($comments)
    {
        $this->commentRepository->deleteAll($comments);
    }

    public function fetchPostComments($postId)
    {
        return $this->commentRepository->findAll(Comment::class,
            [
                'fields' => 'comment.*, user.username, comment_status.status_name',
                'where' => [
                    ['field' => 'post_id',
                        'operator' => '=',
                        'value' => $postId]
                ]
            ],
            [User::class => 'user_id', CommentStatus::class => 'comment_status_id']
        );
    }

}