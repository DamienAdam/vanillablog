<?php

namespace App\Domain\Comment\Type;

use App\Services\Validator\AbstractValidator;

class CommentValidator extends AbstractValidator
{
    public function __construct()
    {
        $this->constraints = [
            'content' => [
                'notNull' => true,
                'minSize' => 2,
                'maxSize' => 500
            ]
        ];
    }
}