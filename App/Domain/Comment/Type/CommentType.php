<?php

namespace App\Domain\Comment\Type;

use App\Services\Type\AbstractType;
use App\Services\Type\SubmitType;
use App\Services\Type\TextAreaType;
use App\Services\Type\TextType;

class CommentType extends AbstractType
{
    public function generateForm()
    {
        $this
            ->add('content', TextAreaType::class, [
                'label' => 'Votre commentaire',
                'class' => ''
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Commenter !',
                'class' => 'btn btn-primary',
            ])
        ;

        return $this->render();
    }
}