<?php

namespace App\Domain\Post;

use App\Core\Application;
use App\Domain\Comment\Entity\Comment;
use App\Domain\Comment\Entity\CommentStatus;
use App\Domain\Comment\Repository\CommentRepository;
use App\Domain\Comment\Repository\CommentStatusRepository;
use App\Domain\Post\Entity\Post;
use App\Domain\Post\Entity\PostStatus;
use App\Domain\Post\Repository\PostRepository;
use App\Domain\Post\Repository\PostStatusRepository;
use App\Domain\User\Entity\User;
use Exception;

/**
 * Class PostManager
 *
 * Interface between repository and application
 *
 * @package App\Domain\Post
 */
class PostManager
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    public function __construct()
    {
        $this->postRepository = new PostRepository();
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function findFirstFromId($id)
    {
        return $this->postRepository->findFirst(Post::class, [
            'fields' => 'post.*, user.username',
            'where' => [
                ['field' => 'post.id',
                'operator' => '=',
                'value' => $id]
            ],
        ], [User::class => 'author_id']);
    }

    /**
     * @param $postId
     * @return array
     * @throws Exception
     */
    public function fetchPostValidatedComments($postId)
    {
        $commentRepository = new CommentRepository();
        $commentStatusRepository = new CommentStatusRepository();

        /** @var CommentStatus $commentStatus */
        $validStatus = $commentStatusRepository->findFirst(CommentStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => 'valid']
        ]]);

        return $commentRepository->findAll(Comment::class, [
            'where' => [
                ['field' => 'comment.post_id',
                'operator' => '=',
                'value' => $postId],
                ['field' => 'comment.comment_status_id',
                'operator' => '=',
                'value' => $validStatus->getId()],
            ]
        ], [
            User::class => 'user_id',
            CommentStatus::class => 'comment_status_id'
        ]);
    }

    public function delete(Post $post)
    {
        $this->postRepository->delete($post);
    }

    /**
     * @param Post $post
     * @throws Exception
     */
    public function create(Post $post)
    {
        $post->setAuthorId(Application::$request->session('user')->getId());

        $statusRepository = new PostStatusRepository();
        $status = $statusRepository->findFirst(PostStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => 'draft']
        ]]);

        $post->setPostStatusId($status->getId());

        $this->postRepository->create($post);
    }

    /**
     * @param Post $post
     * @throws Exception
     */
    public function publish(Post $post)
    {
        $postStatusRepository = new PostStatusRepository();

        /** @var PostStatus $publishedStatus */
        $publishedStatus = $postStatusRepository->findFirst(PostStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => 'active']
        ]]);

        $post->setPostStatusId($publishedStatus->getId());
        $this->postRepository->updateObject(Post::class, $post->getId(), ['post_status_id'], [$publishedStatus->getId()]);
    }

    /**
     * @param Post $post
     * @throws Exception
     */
    public function unpublish(Post $post)
    {
        $postStatusRepository = new PostStatusRepository();

        /** @var PostStatus $unpublishedStatus */
        $unpublishedStatus = $postStatusRepository->findFirst(PostStatus::class, ['where' => [
            ['field' => 'status_name',
            'operator' => '=',
            'value' => 'draft']
        ]]);

        $post->setPostStatusId($unpublishedStatus->getId());
        $this->postRepository->updateObject(Post::class, $post->getId(), ['post_status_id'], [$unpublishedStatus->getId()]);
    }

    public function updateObject($id)
    {
        $this->postRepository->updateObject(Post::class, $id, ['title', 'excerpt', 'content'], Application::$request->post());
    }

    public function findAllActiveWithLimit(int $maxResults)
    {
        return $this->postRepository->findAll(Post::class, [
            'fields' => 'post.*, user.username, post_status.status_name',
            'where' => [
                ['field' => 'post_status.status_name',
                    'operator' => '=',
                    'value' => 'active']
            ],
            'order' => [
                'attribute' => 'updatedAt',
                'way' => 'DESC'
            ],
            'limit' => [
                'start' => 0,
                'amount' => $maxResults
            ]
        ], [User::class => 'author_id', PostStatus::class => 'post_status_id']);
    }


    public function findAllActive()
    {
        return $this->postRepository->findAll(Post::class, [
            'fields' => 'post.*, user.username, post_status.status_name',
            'where' => [
                ['field' => 'post_status.status_name',
                    'operator' => '=',
                    'value' => 'active']
            ],
            'order' => [
                'attribute' => 'updatedAt',
                'way' => 'DESC'
            ]
        ], [User::class => 'author_id', PostStatus::class => 'post_status_id']);
    }
}