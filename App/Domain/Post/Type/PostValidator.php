<?php

namespace App\Domain\Post\Type;

use App\Services\Validator\AbstractValidator;

class PostValidator extends AbstractValidator
{
    public function __construct()
    {
        $this->constraints = [
            'title' => [
                'notNull' => true,
                'minSize' => 6,
                'maxSize' => 200
            ],
            'excerpt' => [
                'notNull' => true,
                'minSize' => 6,
                'maxSize' => 500
            ],
            'content' => [
                'notNull' => true,
                'minSize' => 60,
                'maxSize' => 20000
            ]
        ];
    }

}

