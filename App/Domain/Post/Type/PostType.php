<?php

namespace App\Domain\Post\Type;

use App\Services\Type\AbstractType;
use App\Services\Type\SubmitType;
use App\Services\Type\TextAreaType;
use App\Services\Type\TextType;

class PostType extends AbstractType
{
    public function generateForm()
    {
        $this
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'class' => ''
            ])
            ->add('excerpt', TextType::class, [
                'label' => 'Chapô',
                'class' => ''
            ])
            ->add('content', TextAreaType::class, [
                'label' => 'Contenu',
                'class' => '',
                'wisywig' =>true
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'class' => 'btn btn-primary',
            ])
        ;

        return $this->render();
    }
}