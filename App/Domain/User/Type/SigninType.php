<?php


namespace App\Domain\User\Type;

use App\Services\Type\AbstractType;
use App\Services\Type\PasswordType;
use App\Services\Type\SubmitType;
use App\Services\Type\TextType;

/**
 * Class SigninType
 *
 * Form used to create an account
 *
 * @package App\Domain\User\Type
 */
class SigninType extends AbstractType
{
    public function generateForm()
    {
        $this
            ->add('username', TextType::class, [
                'label' => 'Nom d\'utilisateur',
                'class' => ''
            ])
            ->add('email', TextType::class, [
                'label' => 'Email',
                'class' => ''
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Mot de passe',
                'class' => '',
                'confirm' => [
                    'label' => 'Confirmez le mot de passe'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'class' => 'btn btn-primary',
            ])
        ;

        return $this->render();
    }

}