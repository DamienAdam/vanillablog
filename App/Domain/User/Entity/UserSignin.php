<?php

namespace App\Domain\User\Entity;

/**
 * Class UserSignin
 *
 * DTO for user creation
 * Provides random tokens for password salt and confirm signup attributes
 * Fetch default status and role from database
 *
 * @package App\Domain\User\Entity
 */
class UserSignin
{
    public static $noPersist = [
        'updated_at',
        'created_at',
        'userStatus',
        'userRole',
        'roleName',
        'statusName'
    ];

    private $username;
    private $email;
    private $password;
    private $passwordHash;
    private $passwordSalt;
    private $confirmSignupToken;
    private $userStatusId;
    private $userRoleId;

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return mixed
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @param mixed $passwordSalt
     */
    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = $passwordSalt;
    }

    /**
     * @return mixed
     */
    public function getConfirmSignupToken()
    {
        return $this->confirmSignupToken;
    }

    /**
     * @param mixed $confirmSignupToken
     */
    public function setConfirmSignupToken($confirmSignupToken)
    {
        $this->confirmSignupToken = $confirmSignupToken;
    }

    /**
     * @return mixed
     */
    public function getUserStatusId()
    {
        return $this->userStatusId;
    }

    /**
     * @param mixed $userStatusId
     */
    public function setUserStatusId($userStatusId)
    {
        $this->userStatusId = $userStatusId;
    }

    /**
     * @return mixed
     */
    public function getUserRoleId()
    {
        return $this->userRoleId;
    }

    /**
     * @param mixed $userRoleId
     */
    public function setUserRoleId($userRoleId)
    {
        $this->userRoleId = $userRoleId;
    }

}