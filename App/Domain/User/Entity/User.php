<?php

namespace App\Domain\User\Entity;

use App\Services\Entity\AbstractEntity;

class User extends AbstractEntity
{
    public static $noPersist = [
        'updated_at',
        'created_at',
        'userStatus',
        'userRole',
        'role_name',
        'status_name',
        'csrf_token'
    ];

    private $id;
    private $username;
    private $email;
    private $createdAt;
    private $updatedAt;
    private $resetToken;
    private $confirmSignupToken;
    private $passwordHash;
    private $passwordSalt;
    private $userStatusId;
    private $userRoleId;
    private $roleName;
    private $statusName;
    private $csrfToken;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id): User
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username): User
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email): User
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt): User
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt): User
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResetToken()
    {
        return $this->resetToken;
    }

    /**
     * @param mixed $resetToken
     * @return User
     */
    public function setResetToken($resetToken): User
    {
        $this->resetToken = $resetToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmSignupToken()
    {
        return $this->confirmSignupToken;
    }

    /**
     * @param mixed $confirmSignupToken
     * @return User
     */
    public function setConfirmSignupToken($confirmSignupToken): User
    {
        $this->confirmSignupToken = $confirmSignupToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     * @return User
     */
    public function setPasswordHash($passwordHash): User
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * @param mixed $passwordSalt
     * @return User
     */
    public function setPasswordSalt($passwordSalt): User
    {
        $this->passwordSalt = $passwordSalt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserStatusId()
    {
        return $this->userStatusId;
    }

    /**
     * @param mixed $userStatusId
     * @return User
     */
    public function setUserStatusId($userStatusId): User
    {
        $this->userStatusId = $userStatusId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserRoleId()
    {
        return $this->userRoleId;
    }

    /**
     * @param mixed $userRoleId
     * @return User
     */
    public function setUserRoleId($userRoleId): User
    {
        $this->userRoleId = $userRoleId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * @param mixed $roleName
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;
    }

    /**
     * @return mixed
     */
    public function getStatusName()
    {
        return $this->statusName;
    }

    /**
     * @param mixed $statusName
     */
    public function setStatusName($statusName)
    {
        $this->statusName = $statusName;
    }

    /**
     * @return mixed
     */
    public function getCsrfToken()
    {
        return $this->csrfToken;
    }

    /**
     * @param mixed $csrfToken
     */
    public function setCsrfToken($csrfToken): void
    {
        $this->csrfToken = $csrfToken;
    }
}