<?php

namespace App\Domain\User\Controller;

use App\Core\Application;
use App\Domain\User\Entity\User;
use App\Domain\User\Manager\UserManager;
use App\Domain\User\Type\LoginType;
use App\Domain\User\Type\LoginValidator;
use App\Domain\User\Type\SigninType;
use App\Domain\User\Type\SigninValidator;
use App\Services\Controller\AbstractController;
use Exception;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class SecurityController
 *
 * In charge of the user security
 *
 * @package App\Domain\User\Controller
 */
class SecurityController extends AbstractController
{
    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function signin()
    {
        //On form submit
        if (!empty(Application::$request->post())) {

            $validator = new SigninValidator();

            if ($validator->isValid()) {

                $userManager = new UserManager();

                if (Application::$request->post('username') &&
                    Application::$request->post('email') &&
                    !$userManager->usernameExists(Application::$request->post('username')) &&
                    !$userManager->emailExists(Application::$request->post('email')))
                {
                    $userManager = new UserManager();
                    $user = $userManager->createUser(Application::$request->post('username'),
                        Application::$request->post('email'), Application::$request->post('password'));
                    $userManager->sendConfirmSignupEmail($user);

                    Application::$request->addAlert([
                        'type' => 'success',
                        'title' => 'Inscription',
                        'content' => 'Votre inscription à été prise en compte, un email de confirmation va vous être envoyé'
                    ]);

                    $this->redirect("/");
                }
            }

            Application::$request->addAlert([
                'type' => 'danger',
                'title' => '<i class="fas fa-time"></i> Erreur de formulaire',
                'content' => 'Erreur dans le formulaire, merci de réessayer'
            ]);

            Application::$request->setSession('postdata', Application::$request->post());
            unset($_POST);
            $this->redirect("/signin");

        }

        $form = new SigninType('/signin');

        return $this->render('user/security/signin.html.twig', [
            'form' => $form->generateForm()
        ]);
    }

    /**
     * @param string $token
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function signinConfirm(string $token)
    {
        $userManager = new UserManager();

        /** @var User $user */
        $user = $userManager->findFromConfirmSigninToken($token);

        if ($user->getId()) {
            $userManager->validateUser($user);

            Application::$request->addAlert([
                'type' => 'success',
                'title' => '<i class="fas fa-check"></i> Inscription confirmée',
                'content' => 'Vous êtes désormais un membre validé de ce blog'
            ]);

            return $this->render('user/security/signin-confirm.html.twig');

        }

        Application::$request->addAlert([
            'type' => 'danger',
            'title' => '<i class="fas fa-check"></i> Erreur de validation',
            'content' => 'Une erreur à eu lieu, merci de nous contacter pour résoudre cette anomalie'
        ]);

        return $this->render('user/security/signin-error.html.twig');

    }


    /**
     * @return string|void
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function login()
    {
        //If no submit, form render
        if (empty(Application::$request->post())) {
            $form = new LoginType('/login');

            return $this->render('user/security/login.html.twig', [
                'form' => $form->generateForm()
            ]);
        }

        $validator = new LoginValidator();

        //If form not valid, alert and form render
        if (!$validator->isValid()) {

            Application::$request->addAlert([
                'type' => 'danger',
                'title' => '<i class="fas fa-time"></i> Erreur de formulaire',
                'content' => 'Erreur dans le formulaire'
            ]);

            Application::$request->setSession('postdata', Application::$request->post());
            unset($_POST);
            $this->redirect("/login");
        }

        $userManager = new UserManager();

        //If user not found
        if (Application::$request->post('username') &&
            !$userManager->usernameExists(Application::$request->post('username'))) {

            Application::$request->addAlert([
                'type' => 'danger',
                'title' => '<i class="fas fa-time"></i> Erreur de formulaire',
                'content' => 'Erreur dans vos identifiants, merci de réessayer'
            ]);

            Application::$request->setSession('postdata', Application::$request->post());
            unset($_POST);
            $this->redirect("/login");

        }

        //If password incorrect
        if (Application::$request->post('username') &&
            Application::$request->post('password') &&
            !$userManager->login(Application::$request->post('username'), Application::$request->post('password'))) {
            Application::$request->addAlert([
                'type' => 'danger',
                'title' => '<i class="fas fa-time"></i> Erreur de formulaire',
                'content' => 'Erreur dans vos identifiants, merci de réessayer'
            ]);

            Application::$request->setSession('postdata', Application::$request->post());
            unset($_POST);
            $this->redirect("/login");
        }

        if (Application::$request->post('username') && Application::$request->post('password')) {
            $userManager->login(Application::$request->post('username'), Application::$request->post('password'));
        }

        Application::$request->addAlert([
            'type' => 'success',
            'title' => '<i class="fas fa-check"></i> Connexion',
            'content' => 'Vous êtes connecté(e)'
        ]);

        $this->redirect("/");
    }

    /***
     *
     */
    public function logout()
    {
        $userManager = new UserManager();
        $userManager->logout();

        Application::$request->addAlert([
            'type' => 'danger',
            'title' => '<i class="fas fa-time"></i> Déconnexion',
            'content' => 'Vous êtes déconnecté(e)'
        ]);

        $this->redirect("/");
    }
}