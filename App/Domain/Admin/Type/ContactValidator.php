<?php

namespace App\Domain\Admin\Type;

use App\Services\Validator\AbstractValidator;

class ContactValidator extends AbstractValidator
{
    public function __construct()
    {
        $this->constraints = [
            'fname' => [
                'notNull' => true,
                'minSize' => 2,
                'maxSize' => 200,
            ],
            'lname' => [
                'notNull' => true,
                'minSize' => 2,
                'maxSize' => 200,
            ],
            'email' => [
                'notNull' => true,
                'minSize' => 6,
                'maxSize' => 300,
                'emailValidation' => true
            ],
            'content' => [
                'notNull' => true,
                'minSize' => 6,
                'maxSize' => 2000
            ]
        ];
    }
}