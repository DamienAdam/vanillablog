<?php

namespace App\Domain\Admin\Type;

use App\Services\Type\AbstractType;
use App\Services\Type\SubmitType;
use App\Services\Type\TextType;

class ContactType extends AbstractType
{
    public function generateForm()
    {
        $this
            ->add('fname', TextType::class, [
                'label' => 'Votre prénom',
                'class' => ''
            ])
            ->add('lname', TextType::class, [
                'label' => 'Votre nom',
                'class' => ''
            ])
            ->add('email', TextType::class, [
                'label' => 'Votre email',
                'class' => ''
            ])
            ->add('content', TextType::class, [
                'label' => 'Votre message',
                'class' => '',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer',
                'class' => 'btn btn-primary',
            ]);

        return $this->render();
    }
}