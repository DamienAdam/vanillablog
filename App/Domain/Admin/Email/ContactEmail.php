<?php

namespace App\Domain\Admin\Email;

use App\Core\Application;
use App\Services\Mailer\Mailjet\MJAbstractEmail;
use App\Services\Mailer\Mailjet\MJContact;
use App\Services\Mailer\Mailjet\MJVariable;

class ContactEmail extends MJAbstractEmail
{
     public function __construct()
     {
         //Mailjet template ID
         $this->templateId = 2040603;

         $this->subject = 'Demande de contact';

         $this->to = new MJContact();
         $this->to->name = "Damien Adam";
         $this->to->email = "dodongo_gtr1@hotmail.com";

         if (Application::$request->post('fname') && Application::$request->post('lname')) {
             $this->from = new MJContact();
             $this->from->name = Application::$request->post('fname') . " "
                 . Application::$request->post('lname')
                 . ' via I/O Blog housekeeping';
             $this->from->email = "damien.adam@heureetcontrole.fr";

             $username = new MJVariable();
             $username->key = 'username';
             $username->value = Application::$request->post('fname') . " " .
                 Application::$request->post('lname');
             $this->vars[] = $username;
         }

         if (Application::$request->post('email')) {
             $email = new MJVariable();
             $email->key = 'email';
             $email->value = 'mailto:' . Application::$request->post('email');
             $this->vars[] = $email;
         }

         if (Application::$request->post('content')) {
             $content = new MJVariable();
             $content->key = 'content';
             $content->value = Application::$request->post('content');
             $this->vars[] = $content;
         }
     }

}