<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

use App\Core\Application;
use App\Core\Router\RouterException;

require_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/../');
$dotenv->load();

try {
    new Application(dirname(__DIR__));
} catch (RouterException $e) {
    throw new RouterException('Erreur : ' . $e->getMessage());
}